import React, { Component, Fragment } from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import PostForm from "./PostForm";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
    };
  }
  componentDidMount() {
    fetch("http://localhost:8000/api/post/")
      .then((res) => res.json())
      .then((data) => this.setState({ posts: data }));
  }
  // Citation: Recevied assistance from Matt P. and Peter M. 
  // Thanks to them both for their assistance with the upvote setup
  upvote = (postId) => {
    fetch(`http://localhost:8000/api/post/${postId}/upvote/`, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
    },
    }).then(res => {res.json()
    }).then((data) => window.location.reload())
  }
  
  downvote = (postId) => {
    fetch(`http://localhost:8000/api/post/${postId}/downvote/`, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
    },
    }).then(res => {res.json()
    }).then((data) => window.location.reload())
  }

  render() {
    // Citation: https://www.youtube.com/watch?v=93p3LxR9xfM
    const postsList = this.state.posts.map((post) => (
      <div key={post.id}>
        <ul>
          <h3>{post.b_or_r ? "Boast" : "Roast"}</h3>
          <li>{post.content}</li>
          <li>Upvotes: {post.up_vote}</li>
          <li>Downvotes: {post.down_vote}</li>
          <li>Written: {post.created}</li>
          <li>Total Votes: {post.total_vote}</li>
          <button onClick={() => this.upvote(post.id)}>Upvote</button>
          <button onClick={() => this.downvote(post.id)}>Downvote</button>
        </ul>
      </div>
    ));
    const boastList = this.state.posts
      .filter((post) => post.b_or_r === true)
      .map((post) => (
        <div key={post.id}>
          <ul>
            <h3>Boast</h3>
            <li>{post.content}</li>
            <li>Upvotes: {post.up_vote}</li>
            <li>Downvotes: {post.down_vote}</li>
            <li>Written: {post.created}</li>
            <li>Total Votes: {post.total_vote}</li>
            <button onClick={() => this.upvote(post.id)}>Upvote</button>
          <button onClick={() => this.downvote(post.id)}>Downvote</button>
          </ul>
        </div>
      ));
    const roastList = this.state.posts.filter((post) => post.b_or_r === false).map((post) => (
        <div key={post.id}>
          <ul>
            <h3>Roast</h3>
            <li>{post.content}</li>
            <li>Upvotes: {post.up_vote}</li>
            <li>Downvotes: {post.down_vote}</li>
            <li>Written: {post.created}</li>
            <li>Total Votes: {post.total_vote}</li>
            <button onClick={() => this.upvote(post.id)}>Upvote</button>
          <button onClick={() => this.downvote(post.id)}>Downvote</button>
          </ul>
        </div>
      ));
      // Citation: Fixed a bug with highestRatedList:
      // http://www.javascriptkit.com/javatutors/arraysort2.shtml
    const highestRatedList = this.state.posts.sort(function(a, b){
      return b.total_vote-a.total_vote
  }).map((post) => (
        <div key={post.id}>
          <ul>
            <h3>{post.b_or_r ? "Boast" : "Roast"}</h3>
            <li>{post.content}</li>
            <li>Upvotes: {post.up_vote}</li>
            <li>Downvotes: {post.down_vote}</li>
            <li>Written: {post.created}</li>
            <li>Total Votes: {post.total_vote}</li>
            <button onClick={() => this.upvote(post.id)}>Upvote</button>
          <button onClick={() => this.downvote(post.id)}>Downvote</button>
          </ul>
        </div>
      ));
    return (
      // Citation: Used this to setup Routing: 
      // https://www.freecodecamp.org/news/
      // a-complete-beginners-guide-to-react-router-include-router-hooks/
      <Router>
        <main>
          <nav>
            <Link to="/">All Posts</Link> &nbsp;
            <Link to="/boasts">Filter By Boasts</Link> &nbsp;
            <Link to="/roasts"> Filter By Roasts</Link> &nbsp;
            <Link to="/most_popular">Most Popular</Link> &nbsp;
            <Link to="/create_post">Create New Post</Link> &nbsp;
          </nav>
  <Switch>
    <Route path="/" exact render={() => <Fragment>{postsList}</Fragment>} />
    <Route path="/boasts" render={() => <Fragment>{boastList}</Fragment>} />
    <Route path="/roasts" render={() => <Fragment>{roastList}</Fragment>} />
    <Route path="/most_popular" render={() => <Fragment>{highestRatedList}</Fragment>} />
    <Route path="/create_post" render={() => <PostForm />} />
  </Switch>
        </main>
      </Router>
    );
  }
}

export default App;
