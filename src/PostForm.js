import React, { Component } from "react";

// Citation: Received help from Matt P. and this article
// https://medium.com/@everdimension/
// how-to-handle-forms-with-just-react-ac066c48bd4f
class PostForm extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this)
    }

  handleSubmit(event) {
    event.preventDefault()
    const data = new FormData(event.target);

    fetch('http://localhost:8000/api/post/',{
        method: 'POST',
        body: data,
    }).then((data) => window.location.reload());
  };


  render() {
    return (
      <div>
        <h3>Create Post</h3>
        <form onSubmit={this.handleSubmit}>
          <div>
            <label htmlFor="b_or_r">Boast or Roast</label>
            <input type="checkbox" name="b_or_r" />
          </div>
          <br/>
          <div>
            <label>Content</label>
            <textarea name="content" />
          </div>
          <br />
          <button>Submit</button>
        </form>
      </div>
    );
  }
}

export default PostForm;
